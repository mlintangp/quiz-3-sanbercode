<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuController extends Controller
{
    public function create() {
        return view('buku.create');
    }

    public function store(Request $request) {
        $request->validate([
            'judul' => 'required|unique:buku,judul|max:255',
            'deskripsi' => 'required',
            'pengarang' => 'required',
            'tahun' => 'required'
        ]);
        
        DB::table('buku')->insert(
            [
                'judul' => $request['judul'],
                'deskripsi' => $request['deskripsi'],
                'pengarang' => $request['pengarang'],
                'tahun' => $request['tahun']
            ]
        );

        return redirect('/buku');

    }

    public function index() {
        $buku = DB::table('buku')->get();

        return view('buku.index', compact('buku'));
    }
    
    public function show($id) {
        $buku = DB::table('buku')->where('id', $id)->first();
        return view('buku.show', compact('buku'));
    }

    public function edit($id) {
        $buku = DB::table('buku')->where('id', $id)->first();
        return view('buku.edit', compact('buku'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'judul' => 'required|unique:buku,judul|max:255',
            'deskripsi' => 'required',
            'pengarang' => 'required',
            'tahun' => 'required'
        ]);
        
        DB::table('buku')
            ->where('id', $id)
            ->update(
                [
                    'judul' => $request['judul'],
                    'deskripsi' => $request['deskripsi'],
                    'pengarang' => $request['pengarang'],
                    'tahun' => $request['tahun']
                ]
            );
        
        return redirect('/buku');
    }

    public function destroy($id) {
        DB::table('buku')->where('id', '=', $id)->delete();
        
        return redirect('/buku');
    }

}
