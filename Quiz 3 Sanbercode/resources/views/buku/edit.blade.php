@extends('layouts.master')

@section('title')
    Edit Data Buku
@endsection

@section('content')

<div class="ml-3">
    <div class="card mt-3">
        <div class="card-header">
            <h3 class="card-title">Edit Data Buku</h3>
        </div>
        <div class="card-body">
            <form action="/buku/{{$buku->id}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" value="{{$buku->judul}}" class="form-control" name="judul" id="judul" placeholder="Masukkan judul">
                    @error('judul')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <textarea name="deskripsi" value="{{$buku->deskripsi}}" class="form-control" cols="30" rows="10"></textarea>
                    @error('deskripsi')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tahun">Pengarang</label>
                    <input type="text" value="{{$buku->pengarang}}" class="form-control" name="pengarang" id="pengarang" placeholder="Masukkan pengarang">
                    @error('pengarang')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="text" value="{{$buku->tahun}}" class="form-control" name="tahun" id="tahun" placeholder="Masukkan tahun">
                    @error('tahun')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
    </div>
</div>

@endsection