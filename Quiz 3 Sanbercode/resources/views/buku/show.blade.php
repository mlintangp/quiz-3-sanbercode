@extends('layouts.master')

@section('title')
    Detail Buku
@endsection

@section('content')

<div class="ml-3">
    <div class="card mt-3">
        <div class="card-header">
            <h3 class="card-title">Detail Buku</h3>
        </div>
        <div class="card-body">
            <h1 style="color:violet">{{ $buku->judul }}</h1>
            <p>{{ $buku->deskripsi }}</p>
            <p>Pengarang: {{ $buku->pengarang }}</p>
            <p>Tahun Terbit: {{ $buku->tahun }}</p>
            <p>Genre:</p>
            <ul>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </div>
</div>

@endsection