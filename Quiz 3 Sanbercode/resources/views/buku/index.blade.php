@extends('layouts.master')

@section('title')
    Data Buku
@endsection

@section('content')

<div class="ml-3">
    <div class="card mt-3">
        <div class="card-header">
            <h3 class="card-title">Data Buku</h3>
        </div>
        <div class="card-body">  
            <a href="/buku/create" class="btn btn-success btn-sm">Tambah Data Buku</a>
            <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Pengarang</th>
                <th scope="col">Tahun</th>
                <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($buku as $key => $c)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $c->judul }}</td>
                        <td>{{ $c->deskripsi }}</td>
                        <td>{{ $c->pengarang }}</td>
                        <td>{{ $c->tahun }}</td>
                        <td>
                            <form action="/buku/{{ $c->id }}" method="POST">
                                @csrf
                                @method('delete')
                                <a href="/buku/{{ $c->id }}" class="btn btn-info btn-sm">Details</a>
                                <a href="/buku/{{ $c->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>

                @empty
                    <tr>
                        <td>Tidak ada data yang dapat ditampilkan.</td>
                    </tr>
                @endforelse
            </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script src="{{ asset('/sbadmin2/js/swal.min.js') }}"></script>
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>

@endpush